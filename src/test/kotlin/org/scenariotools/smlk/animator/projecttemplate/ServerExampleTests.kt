import org.scenariotools.smlk.animator.projecttemplate.cases.fourservers.configurationFourServers
import org.scenariotools.smlk.runTest
import org.junit.jupiter.api.Test
import org.scenariotools.smlk.animator.projecttemplate.cases.twoservers.configurationTwoServers

class ServerExampleTests {

    @Test
    fun testScenariosTwoServers() = configurationTwoServers().second.test()

    @Test
    fun testScenariosFourServers() = configurationFourServers().second.test()

}
