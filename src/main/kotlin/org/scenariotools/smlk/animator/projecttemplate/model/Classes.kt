package org.scenariotools.smlk.animator.projecttemplate.model

import javafx.beans.binding.StringBinding
import javafx.beans.property.SimpleMapProperty
import org.scenariotools.smlk.event
import org.scenriotools.smlk.animator.model.Instance
import tornadofx.c
import tornadofx.toObservable

open class Server(name : String, xPos : Double, yPos : Double)
    : Instance(name, xPos, yPos, width = 200.0, height = 60.0, color = c("#000000"), bgcolor = c("#ffeeaa")), IClient{

    val userIDToUserNameProperty = SimpleMapProperty<String, String>(mapOf<String, String>().toObservable())

    val slaveServers : MutableList<Server> = mutableListOf()
    var masterServer : Server? = null

    override fun getPropertyList(): List<Pair<String, StringBinding>> {
        return  listOf(
            "userNames" to userIDToUserNameProperty.asString()
        )
    }

    fun getUserName(userID: String) = event(userID){}

    fun putUserName(userID: String, name: String) = event(userID, name){
        userIDToUserNameProperty.put(userID, name)
    }

}

open class Client(name : String, xPos : Double, yPos : Double)
    : Instance(name, xPos, yPos, width = 200.0, height = 60.0, color = c("#000000"), bgcolor = c("#eeffaa")), IClient {
    var server : Server? = null

}

interface IClient{
    fun serverResponse(message: String) = event(message){}

}