package org.scenariotools.smlk.animator.projecttemplate.cases

import org.scenariotools.smlk.animator.projecttemplate.cases.fourservers.configurationFourServers
import org.scenariotools.smlk.animator.projecttemplate.cases.twoservers.configurationTwoServers
import org.scenriotools.smlk.animator.model.InteractionSystemConfigurations

val configurations = InteractionSystemConfigurations(
        mapOf(
                configurationFourServers(),
                configurationTwoServers()
        )
)