package org.scenariotools.smlk.animator.projecttemplate.cases.fourservers

import org.scenariotools.smlk.animator.projecttemplate.model.*
import org.scenariotools.smlk.symbolicEvent
import org.scenriotools.smlk.animator.model.Instance
import org.scenriotools.smlk.animator.model.InteractionSystemConfiguration
import org.scenriotools.smlk.animator.smlkextension.shadowScenario


object Server1 : Server("Server 1", 100.0, 100.0){}

object Server2 : Server("Server 2", 100.0, 300.0){}

object Server3 : Server("Server 3", 100.0, 500.0){}

object Server4 : Server("Server 4", 400.0, 500.0){}

object Client1 : Client("Client", 500.0, 100.0){}

val instances = listOf<Instance>(
        Client1,
        Server1,
        Server2,
        Server3,
        Server4)


fun configurationFourServers() : Pair<String, InteractionSystemConfiguration>{

    Client1.dependencies.add(Server1)
    Client1.server = Server1
    Server1.dependencies.add(Server2)
    Server2.dependencies.add(Server1)
    Server2.dependencies.addAll(Server3, Server4)
    Server3.dependencies.add(Server2)
    Server4.dependencies.add(Server2)

    Server1.slaveServers.add(Server2)
    Server2.masterServer = Server2
    Server2.slaveServers.add(Server3)
    Server3.masterServer = Server3
    Server2.slaveServers.add(Server4)
    Server4.masterServer = Server4

    val configuration = InteractionSystemConfiguration(
            1900.0, 900.0,
            environmentObjects = setOf(Client1),
            eventsNotToVisualize = Instance::setShadowSize.symbolicEvent(),
            instances = instances,
            testScenario = testScenariosFourServers,
            guaranteeScenarios = setOf(scenarios_v1, setOf(shadowScenario)).flatten().toSet()
    )

    return "Simple Test Interaction System -- FourServers" to configuration
}


