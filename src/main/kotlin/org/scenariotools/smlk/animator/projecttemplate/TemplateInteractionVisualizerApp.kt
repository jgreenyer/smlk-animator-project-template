package org.scenariotools.smlk.animator.projecttemplate

import javafx.stage.Stage
import org.scenariotools.smlk.ScenarioProgram
import org.scenariotools.smlk.animator.projecttemplate.cases.configurations
import org.scenariotools.smlk.animator.projecttemplate.model.scenarios_v3
import org.scenriotools.smlk.animator.style.InteractionAnimatorStyles
import org.scenriotools.smlk.animator.view.InteractionAnimatorView
import tornadofx.App
import tornadofx.find
import tornadofx.launch


fun main(args: Array<String>) {
    launch<InteractionVisualizerApp>(*args)
}


class InteractionVisualizerApp: App(InteractionAnimatorView::class, InteractionAnimatorStyles::class){

    override fun start(stage: Stage) {
        super.start(stage)

        val view = find(primaryView, scope)

        val interactionAnimatorView = view as InteractionAnimatorView
        val interactionSystem = interactionAnimatorView.isvm.item

        interactionSystem.updateInteractionSystemConfigurations(configurations)

    }

}
