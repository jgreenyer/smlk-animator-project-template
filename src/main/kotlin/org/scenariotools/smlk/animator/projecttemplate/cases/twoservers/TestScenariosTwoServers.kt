package org.scenariotools.smlk.animator.projecttemplate.cases.twoservers

import org.scenariotools.smlk.scenario
import org.scenariotools.smlk.sends
import org.scenriotools.smlk.animator.smlkextension.label

val testScenariosTwoServers =
    scenario{
        label("Init")
        request(Client1 sends Server1.putUserName("u123", "Adam"))
        request(Client1 sends Server2.putUserName("u234", "Eve"))
        label("Get User Name for User ID u123")
        request(Client1 sends Server1.getUserName("u123"))
        mustOccur(Server1 sends Client1.serverResponse("u123:Adam"))
        label("Get User Name for User ID u234")
        request(Client1 sends Server1.getUserName("u234"))
        mustOccur(Server1 sends Client1.serverResponse("u234:Eve"))
        label("Get User Name for User ID u345")
        request(Client1 sends Server1.getUserName("u345"))
        mustOccur(Server1 sends Client1.serverResponse("u345:<Unknown>"))
    }
