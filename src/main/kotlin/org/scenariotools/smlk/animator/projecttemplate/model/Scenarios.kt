package org.scenariotools.smlk.animator.projecttemplate.model

import org.scenariotools.smlk.*
import org.scenriotools.smlk.animator.smlkextension.instancelabel



val scenarios_v1 = listOf<suspend Scenario.() -> Unit>(

    scenario(Server::getUserName.symbolicEvent()){

        val requestingClient = it.sender as IClient
        val userID = it.parameters[0] as String
        val server = it.receiver
        val userName = server.userIDToUserNameProperty[userID]

        if(userName != null){
            instancelabel(server, "Username found for $userID")
            request(server sends requestingClient.serverResponse("$userID:$userName"))
        }else{
            for(slaveServer in server.slaveServers){
                request(server sends slaveServer.getUserName(userID))
                val responseEvent = waitFor(ANY sends server receives IClient::serverResponse)
                val responseValue = responseEvent.parameters[0] as String
                if (!responseValue.endsWith("<Unknown>")) {
                    request(server sends requestingClient.serverResponse(responseValue))
                    terminate() // do not wait for further slave server responses
                }
            }
            instancelabel(server, "No slave server knows $userID")
            request(server sends requestingClient.serverResponse("$userID:<Unknown>"))
        }
    }
)

val scenarios_v2 = listOf<suspend Scenario.() -> Unit>(

    scenario(ANY sends Server::getUserName.symbolicEvent()){
        val requestingClient = it.sender as IClient
        val userID = it.parameters[0] as String
        val server = it.receiver
        val userName = server.userIDToUserNameProperty[userID]

        scenario {
            if(userName == null){
                instancelabel(server, "No username for $userID")

                server receives IClient::serverResponse forbiddenIn scenario {
                    for(slaveServer in server.slaveServers){
                        instancelabel(server, "Asking slaveServer ${slaveServer.name} for username of $userID")
                        request(server sends slaveServer.getUserName(userID))
                    }
                }
                // requests must be sent to the slave servers before their responses are processed (see next step)
                // (if this was omitted, we might miss serverResponse events and the scenario would get stuck in the
                // following repeat loop)

                repeat(server.slaveServers.size){
                    instancelabel(server,"Waiting for slave server response.")
                    val responseEvent = waitFor(ANY sends server receives IClient::serverResponse)
                    val responseValue = responseEvent.parameters[0] as String
                    if (!responseValue.endsWith("<Unknown>")) {
                        instancelabel(server, "Forwarding user name of $userID received from slave")
                        request(server sends requestingClient.serverResponse(responseValue))
                        terminate() // do not wait for further slave server responses
                    }
                }

                instancelabel(server, "No slave server knows $userID")
                request(server sends requestingClient.serverResponse("$userID:<Unknown>"))
            }else{
                instancelabel(server, "Username found for $userID")
                request(server sends requestingClient.serverResponse("$userID:$userName"))
            }
        } before (server receives Server::getUserName)
        // this scenario fragment must happen before the next getUserName request is received by the server
        // (if this was omitted, two instance of this scenario could be started concurrently for the same server,
        // and their events might interfere.)

    }
)


val scenarios_v3 = listOf<suspend Scenario.() -> Unit>(

    scenario(ANY sends Server::getUserName.symbolicEvent()){
        val requestingClient = it.sender as IClient
        val userID = it.parameters[0] as String
        val server = it.receiver
        val userName = server.userIDToUserNameProperty[userID]

        if(userName == null){
            instancelabel(server, "No username for $userID")

            for(slaveServer in server.slaveServers){
                instancelabel(server, "Asking slaveServer ${slaveServer.name} for username of $userID")
                request(server sends slaveServer.getUserName(userID))
            }

            var responseCounter = server.slaveServers.size
            while(responseCounter > 0){
                instancelabel(server,"Waiting for slave server response.")
                val responseEvent = waitFor(ANY sends server receives IClient::serverResponse)
                val responseValue = responseEvent.parameters[0] as String
                if (responseValue.startsWith(userID)){
                    responseCounter--
                    if (!responseValue.endsWith("<Unknown>")) {
                        instancelabel(server, "Forwarding user name of $userID received from slave")
                        request(server sends requestingClient.serverResponse(responseValue))
                        terminate() // do not wait for further slave server responses
                    }
                }
            }

            instancelabel(server, "No slave server knows $userID")
            request(server sends requestingClient.serverResponse("$userID:<Unknown>"))
        }else{
            instancelabel(server, "Username found for $userID")
            request(server sends requestingClient.serverResponse("$userID:$userName"))
        }

    }

)
