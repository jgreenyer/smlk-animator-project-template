package org.scenariotools.smlk.animator.projecttemplate.cases.twoservers

import org.scenariotools.smlk.animator.projecttemplate.model.Client
import org.scenariotools.smlk.animator.projecttemplate.model.Server
import org.scenariotools.smlk.animator.projecttemplate.model.scenarios_v3
import org.scenariotools.smlk.symbolicEvent
import org.scenriotools.smlk.animator.model.Instance
import org.scenriotools.smlk.animator.model.InteractionSystemConfiguration
import org.scenriotools.smlk.animator.smlkextension.shadowScenario


object Server1 : Server("Server 1", 100.0, 100.0){}

object Server2 : Server("Server 2", 100.0, 300.0){}

object Client1 : Client("Client", 500.0, 100.0){}

val instances = listOf<Instance>(
        Client1,
        Server1,
        Server2)


fun configurationTwoServers() : Pair<String, InteractionSystemConfiguration>{

    Client1.dependencies.add(Server1)
    Client1.server = Server1

    Server1.dependencies.add(Server2)
    Server2.dependencies.add(Server1)

    Server1.slaveServers.add(Server2)
    Server2.masterServer = Server1


    val configuration = InteractionSystemConfiguration(
            1900.0, 900.0,
            environmentObjects = setOf(Client1),
            eventsNotToVisualize = Instance::setShadowSize.symbolicEvent(),
            instances = instances,
            testScenario = testScenariosTwoServers,
            guaranteeScenarios = setOf(scenarios_v3, setOf(shadowScenario)).flatten().toSet()
    )

    return "Simple Test Interaction System -- TwoServers" to configuration
}

